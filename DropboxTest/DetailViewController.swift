//
//  DetailViewController.swift
//  DropboxTest
//
//  Created by SITF NNagavci on 24.08.17.
//  Copyright © 2017 N Apps. All rights reserved.
//

import UIKit
import MessageUI
import AVKit
import NVActivityIndicatorView
import SwiftyDropbox

class DetailViewController: UIViewController,MFMailComposeViewControllerDelegate,UIWebViewDelegate {
// Item type 2. Music 3. Image 4. Document
    
    //outlets
    
    @IBOutlet var lblCurrentTime: UILabel!
    @IBOutlet var lblTotalTime: UILabel!
    @IBOutlet var btnPlay: UIButton!
    @IBOutlet var slider: UISlider!
    @IBOutlet var btnEmail: UIBarButtonItem!
    
    //locals
    public var item:Items!
    private var player:AVPlayer!
    private var timer = Timer()
    private let activityIndicator = NVActivityIndicatorView.init(frame: CGRect(x: 0, y: 0, width: 100.0, height: 100.0), type: NVActivityIndicatorType.ballClipRotate, color: .black, padding: CGFloat.leastNormalMagnitude)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator.startAnimating()
        activityIndicator.center = view.center
        
        
        if item.type != 2 {
            //documents and images
            view.addSubview(activityIndicator)
            let webV:UIWebView = UIWebView.init(frame: CGRect(x: 0, y: 0, width: view.bounds.size.width, height: view.bounds.size.height))
            webV.loadRequest(NSURLRequest(url: NSURL(string: item.tempLink!)! as URL) as URLRequest)
            webV.delegate = self as? UIWebViewDelegate;
            self.view.addSubview(webV)
        }else{
            self.navigationItem.rightBarButtonItem = nil
            slider.addTarget(self, action: #selector(self.sliderValueChanged), for: UIControlEvents.valueChanged)
            play(url: NSURL(string: item.tempLink!)!)
        }
        view.bringSubview(toFront: activityIndicator)
        // Do any additional setup after loading the view.
    }
    // MARK : WebView Delegate
    func webViewDidFinishLoad(_ webView: UIWebView) {
        activityIndicator.stopAnimating()
    }
    // MARK : Audio Player
    func play(url:NSURL) {
        let playerItem = AVPlayerItem(url: url as URL)
        self.player = AVPlayer(playerItem:playerItem)
        player.volume = 1.0
    
        let audioDuration = player.currentItem?.asset.duration
        let audioDurationSeconds = CMTimeGetSeconds(audioDuration!)
        slider.maximumValue = Float(audioDurationSeconds)
        lblTotalTime.text = String(format: "%02d:%02d", Int(audioDurationSeconds / 60), Int(audioDurationSeconds.truncatingRemainder(dividingBy: 60)))
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying(note:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
        player.play()
        activityIndicator.stopAnimating()
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateTime), userInfo: nil, repeats: true)
        timer.fire()
    }
    func sliderValueChanged(){
        player.seek(to: CMTime(seconds: Double(slider.value), preferredTimescale: 1))
    }
    func updateTime() {
        if player.isPlaying {
            let currentTime = CMTimeGetSeconds(player.currentTime())
            if currentTime != 0{
                slider.value = slider.value + 1
            }
            lblCurrentTime.text = String(format: "%02d:%02d", Int(currentTime / 60), Int(currentTime.truncatingRemainder(dividingBy: 60)))
        }
    }
    func playerDidFinishPlaying(note: NSNotification) {
        slider.value = 0
        lblCurrentTime.text = "00:00"
        timer.invalidate()
        btnPlay.setImage(UIImage.init(named: "play"), for: .normal)
        player.seek(to: CMTime(seconds: 0, preferredTimescale: 1))
    }
    // MARK: - Actions
    @IBAction func dismiss(_ sender: Any) {
        timer.invalidate()
        if player != nil{
            player.pause()
            player = nil
        }
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func playClicked(_ sender: Any) {
        if player.isPlaying {
            player.pause()
            btnPlay.setImage(UIImage.init(named: "play"), for: .normal)
        }else{
            player.play()
            btnPlay.setImage(UIImage.init(named: "pause"), for: .normal)
        }
    }
    // MARK : Email
    @IBAction func email(_ sender: Any) {
        //Check to see the device can send email.
        btnEmail.isEnabled = false
        activityIndicator.startAnimating()
        if( MFMailComposeViewController.canSendMail() ) {
            let client = DropboxClientsManager.authorizedClient as DropboxClient!
            client?.files.download(path: item.path!).response { (response, error) in
                if response != nil {
                    let fileData = response?.1
                    
                    
                    print("Can send email.")
                    
                    let mailComposer = MFMailComposeViewController()
                    mailComposer.mailComposeDelegate = self
                    
                    //Set the subject and message of the email
                    mailComposer.setSubject("File from Dropbox")
                    mailComposer.setMessageBody(self.item.name!, isHTML: false)
                    let fileExtension = (self.item.path!.lowercased() as NSString).pathExtension
                    
                    mailComposer.addAttachmentData(fileData!, mimeType: fileExtension, fileName: self.item.path!)
                    self.activityIndicator.stopAnimating()
                    self.btnEmail.isEnabled = true
                    self.present(mailComposer, animated: true, completion: nil)
                    
                }else{
                    self.btnEmail.isEnabled = true
                    self.activityIndicator.stopAnimating()
                    let alertController = UIAlertController(title: "Error", message: "Error while downloading file", preferredStyle: .alert)
                    
                    self.present(alertController, animated: true, completion:nil)
                    
                    let okAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                        
                    }
                    
                    alertController.addAction(okAction)
                }
                
            }
        }else{
            btnEmail.isEnabled = true
            self.activityIndicator.stopAnimating()
            let alertController = UIAlertController(title: "Failed", message: "Failed to send email, check if email configured on iPhone", preferredStyle: .alert)
            
            self.present(alertController, animated: true, completion:nil)
            
            let okAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                
            }
            
            alertController.addAction(okAction)
            
        }
        
    }
    func showEmailVC() {
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        
        // Configure the fields of the interface.
        composeVC.setToRecipients([""])
        composeVC.setSubject("DropBox")
        composeVC.setMessageBody(item.name!, isHTML: false)
        
        
        // Present the view controller modally.
        self.present(composeVC, animated: true, completion: nil)
    }
    // MARK: Mail Delegate
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AVPlayer {
    var isPlaying: Bool {
        return rate != 0 && error == nil
    }
}
