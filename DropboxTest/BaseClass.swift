//
//  BaseClass.swift
//
//  Created by SITF NNagavci on 23.08.17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class BaseClass: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let items = "Items"
  }

  // MARK: Properties
  public var items: Items?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    items = Items(json: json[SerializationKeys.items])
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = items { dictionary[SerializationKeys.items] = value.dictionaryRepresentation() }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.items = aDecoder.decodeObject(forKey: SerializationKeys.items) as? Items
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(items, forKey: SerializationKeys.items)
  }

}
