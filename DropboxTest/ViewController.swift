//
//  ViewController.swift
//  DropboxTest
//
//  Created by SITF NNagavci on 23.08.17.
//  Copyright © 2017 N Apps. All rights reserved.
//

import UIKit
import SwiftyDropbox
import NVActivityIndicatorView

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    // Item type 1. Folder 2. Music 3. Image 4. Document 5. Others
    // MARK: - Outlets
    @IBOutlet var btnLnkDropbox: UIButton!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var btnLogOut: UIBarButtonItem!
    
    
    // MARK: - Locals
    var client:DropboxClient!
    var dropbox:NSMutableArray! = []
    let defaults:UserDefaults = UserDefaults.standard
    var pathIndex:Int! = 0
    var paths:NSMutableArray! = []
    var selectedIndexPath:IndexPath!
    private let activityIndicator = NVActivityIndicatorView.init(frame: CGRect(x: 0, y: 0, width: 100.0, height: 100.0), type: NVActivityIndicatorType.ballClipRotate, color: .black, padding: CGFloat.leastNormalMagnitude)
    
    // MARK: - Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.automaticallyAdjustsScrollViewInsets = false
        tableView.delegate = self
        tableView.dataSource = self
        activityIndicator.startAnimating()
        activityIndicator.center = view.center
        view.addSubview(activityIndicator)
        layout()
        view.bringSubview(toFront: activityIndicator)
        
        if (DropboxClientsManager.authorizedClient == nil) {
            btnLogOut.isEnabled = false
            // Define identifier
            let notificationName = Notification.Name("layout")
            // Register to receive notification
            NotificationCenter.default.addObserver(self, selector: #selector(self.layout), name: notificationName, object: nil)
        }
    }

    func layout(){
        self.btnLogOut.isEnabled = true
        client = DropboxClientsManager.authorizedClient
        if (client != nil) {
            btnLnkDropbox.isHidden = true
            tableView.isHidden = false
            if paths.count == 0{
                paths.add("");
                dropboxData("", performSegue: "")
            }
        }
    }
    
    func dropboxData(_ searchPath: String, performSegue segue: String) {
        activityIndicator.startAnimating()
        client.files.listFolder(path: searchPath).response { (result, error) in
            if (result != nil) {
                self.displayResults((result?.entries)!)
            }else{
                //error authenticating
                self.btnLnkDropbox.isHidden = false
                self.tableView.isHidden = true
            }
            self.activityIndicator.stopAnimating()
        }
    }
    
    func fileType(_ itemName: String) -> Int {
        // Item type 1. Folder 2. Music 3. Image 4. Document 5. Others
        // check what type of file
        let fileExtension = (itemName.lowercased() as NSString).pathExtension
        if fileExtension == "mp3" {
            return 2
        }else if fileExtension == "png" || fileExtension == "jpeg" || fileExtension == "jpg" || fileExtension == "tiff" || fileExtension == "bmp"{
            return 3
        }else if fileExtension == "pdf" || fileExtension == "doc" || fileExtension == "docx" || fileExtension == "txt" || fileExtension == "xlsx"{
            return 4
        }
        // if other
        return 5
    }

    func displayResults(_ folderEntries: Array<Files.Metadata>) {
        dropbox = []
        for metaData: Files.Metadata in folderEntries {
            if (metaData is Files.FileMetadata) {
                // FILE
                let item = Items.init(object: "")
                item.name = metaData.name
                item.path = metaData.pathLower
                
                item.lastModified = metaData.modifiedDate?.toString(dateFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX")
                item.type = fileType(metaData.name)

                dropbox.add(item)
            }
            else if (metaData is Files.FolderMetadata) {
                    // is a folder
                    let item = Items.init(object: "")
                    item.name = metaData.name
                    item.path = metaData.pathLower
                    item.lastModified = metaData.modifiedDate?.toString(dateFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX")
                    item.type = 1
                    dropbox.add(item)
            }
        }
        tableView.reloadData()
    }
    
    func leftBarButton(_ count: Int) {
        if count == 0 {
            self.navigationItem.leftBarButtonItem = nil
        }else{
            self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(title: "Back", style: .plain, target: self, action: #selector(back(_:)))
        }
    }
    func segue() {
        activityIndicator.stopAnimating()
        performSegue(withIdentifier: "detailVC", sender: self)
    }
    // MARK: - TableView Delegate/DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dropbox.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        var cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        if cell == nil {
            cell = UITableViewCell(style: .value1, reuseIdentifier: "reuseIdentifier")
        }
        let item:Items = dropbox.object(at: indexPath.row) as! Items
        cell?.textLabel?.text = item.name
        //check if date is dummy date
        cell?.detailTextLabel?.text = !(item.lastModified?.contains("9999"))! ? item.lastModified : ""
        
        if item.type == 1 {
            cell?.imageView?.image = UIImage.init(named: "folder")
        }else if item.type == 2{
            cell?.imageView?.image = UIImage.init(named: "music")
        }else if item.type == 3{
            cell?.imageView?.image = UIImage.init(named: "image")
        }else if item.type == 4{
            cell?.imageView?.image = UIImage.init(named: "document")
        }else{
            cell?.imageView?.image = UIImage.init(named: "file")
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndexPath = indexPath
        let item:Items = dropbox.object(at: indexPath.row) as! Items
        if item.type == 1 {
            paths.add(item.path!)
            pathIndex = pathIndex + 1
            dropboxData(item.path!, performSegue: "")
            leftBarButton((item.path?.characters.count)!)
        }else{
            if item.type != 5{
                activityIndicator.startAnimating()
                // if item type is not "other" check if tempLink is stored
                if item.tempLink == nil{
                client.files.getTemporaryLink(path: item.path!).response(completionHandler: { (result, error) in
                    if result != nil{
                        item.tempLink = result?.link
                        self.segue()
                    }else{
                        self.activityIndicator.stopAnimating()
                    }
                })
                }else{
                    segue()
                }
            }
        }
    }
    
    // MARK: - Actions
    @IBAction func back(_ sender: Any) {
        paths.removeObject(at: pathIndex)
        pathIndex = pathIndex - 1
        dropboxData(paths.object(at: pathIndex) as! String, performSegue: "")
        leftBarButton(pathIndex)
    }
    
    @IBAction func logOut(_ sender: Any) {
        let alertController = UIAlertController(title: "Log out", message: "Are you sure you want to log out?", preferredStyle: .alert)
        
        self.present(alertController, animated: true, completion:nil)
        
        let yesAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
            self.btnLogOut.isEnabled = false
            let notificationName = Notification.Name("layout")
            // Register to receive notification
            NotificationCenter.default.addObserver(self, selector: #selector(self.layout), name: notificationName, object: nil)
            DropboxClientsManager.unlinkClients()
            self.tableView.isHidden = true
            self.btnLnkDropbox.isHidden = false
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction) in
        }
        
        alertController.addAction(yesAction)
        alertController.addAction(cancelAction)
    }
    
    @IBAction func dropBoxLink(_ sender: Any) {
        DropboxClientsManager.authorizeFromController(UIApplication.shared,
                                                      controller: self,
                                                      openURL: { (url: URL) -> Void in
                                                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
        })
    }
    // MARK : Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailVC"{
            let destinationNavigationController = segue.destination as! UINavigationController
            let targetController = destinationNavigationController.topViewController as! DetailViewController
            targetController.item = dropbox.object(at: selectedIndexPath.row) as! Items
        }
    }
}
extension Date
{
    func toString( dateFormat format  : String ) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let myString = dateFormatter.string(from: self)
        let yourDate = dateFormatter.date(from: myString)
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        return dateFormatter.string(from: yourDate!)
    }
    
}
